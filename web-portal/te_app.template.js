var url = "http://SDN_CONTROLLER:8080/ospf_monitor/get_topology_netjson";
var graph;
d3.netJsonGraph(url,{linkDistance: 50,charge: -130,friction: .9,linkStrength: .2,chargeDistance: Infinity,theta: .2,gravity: .01});
graph = fetch_graph(url);
console.log("Displayed graph: " + graph);



function fetch_graph(url){
        var graph = (function() {
                        var json = null;
                        $.ajax({
                                'async': false,
                                'global': false,
                                'url': url,
                                'dataType': "json",
                                'success': function (data) {
                                        json = data;
                                }
                        });
                        return json;
        })();
        return graph;
}
